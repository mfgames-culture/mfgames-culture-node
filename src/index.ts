import * as Promise from "bluebird";
import * as fs from "fs-extra";
import * as culture from "mfgames-culture";
import * as path from "path";

/**
 * A node-based data provider that pulls data from a directory and uses it
 * to provide the requested data. This implements both the promise- and
 * sync-based implementations. This assumes that a flat directory with each
 * id.json at the top level.
 */
export class DirctoryCultureDataProvider implements
    culture.SyncCultureDataProvider,
    culture.PromiseCultureDataProvider {
    public directory: string;
    private data: any = {};

    constructor(directory?: string) {
        this.directory = directory;
    }

    public getCulturePromise(id: string): Promise<culture.CultureData> {
        // See if we already have the data loaded, if we do then we can
        // return it immediately.
        if (id in this.data) {
            return Promise.resolve(this.data[id] as culture.CultureData);
        }

        // We don't have this already loaded, so we need a promise to load
        // it from the server. This is a more complicated one since each
        // calendar load is also a promise.
        const fileName = this.getFileName(id);

        return fs.readFile(fileName)
            .then((buffer) => JSON.parse(buffer) as culture.CultureData)
            .then((c) => {
                const calPromises = c.instants.refs
                    .map((calData) => this.getCalendarPromise(calData));

                return Promise
                    .all(calPromises)
                    .then((cals) => {
                        c.instants.calendarData = culture.mergeCalendars(cals);
                        return c;
                    });
            })
            .then((c) => {
                const calPromises = c.periods.refs
                    .map((calData) => this.getCalendarPromise(calData));

                return Promise
                    .all(calPromises)
                    .then((cals) => {
                        c.periods.calendarData = culture.mergeCalendars(cals);
                        return c;
                    });
            })
            .then((c) => {
                this.data[id] = c;
                return c;
            });
    }

    public getCultureSync(id: string): culture.CultureData {
        // See if we already have the data loaded, if we do then we can
        // return it immediately.
        if (id in this.data) {
            return Promise.resolve(this.data[id] as culture.CultureData);
        }

        // Load the file into memory, save it to the cache, and return it.
        const fileName = this.getFileName(id);
        const buffer = fs.readFileSync(fileName);
        const results = JSON.parse(buffer) as culture.CalendarData;

        // We need to look through the calendars and resolve them..
        results.instants.calendarData = culture.mergeCalendars(
            results.instants.refs
                .map((c) => this.getCalendarSync(c)));
        results.periods.calendarData = culture.mergeCalendars(
            results.periods.refs
                .map((c) => this.getCalendarSync(c)));

        // Return the resulting data.
        this.data[id] = results;

        return results;
    }

    public getCalendarPromise(id: string): Promise<culture.CalendarData> {
        // See if we already have the data loaded, if we do then we can
        // return it immediately.
        if (id in this.data) {
            return Promise.resolve(this.data[id] as culture.CalendarData);
        }

        // We don't have this already loaded, so we need a promise to load
        // it from the server.
        const fileName = this.getFileName(id);

        return fs.readFile(fileName)
            .then((buffer) => JSON.parse(buffer) as culture.CultureData)
            .then((json) => this.data[id] = json);
    }

    public getCalendarSync(id: string): culture.CalendarData {
        // See if we already have the data loaded, if we do then we can
        // return it immediately.
        if (id in this.data) {
            return Promise.resolve(this.data[id] as culture.CalendarData);
        }

        // Load the file into memory, save it to the cache, and return it.
        const fileName = this.getFileName(id);
        const buffer = fs.readFileSync(fileName);
        const json = JSON.parse(buffer) as culture.CalendarData;

        this.data[id] = json;

        return json;
    }

    /**
     * Gets the filename for a given identifier.
     */
    private getFileName(id: string): string {
        // Make sure we have a valid directory.
        if (!this.directory) {
            throw new Error(
                "directory has not been set. " +
                "Either provide it from the constructor " +
                "(`new DirctoryCultureDataProvider(directoryName);`) " +
                "or set it via property " +
                "(`provider.directory = directoryName;`).");
        }

        // Return the directory filename.
        return path.join(this.directory, id + ".json");
    }
}
