const expect = require("expect");
import * as path from "path";
import * as n from "../index";
import * as culture from "mfgames-culture";
import * as data from "mfgames-culture-data";

describe(path.basename(__filename), function() {
    it("load calendar", function () {
		var provider = new n.DirctoryCultureDataProvider(data.dataDirectory);
		var calendarData = provider.getCalendarSync("gregorian");
		expect(calendarData.id).toEqual("gregorian");

		var calendar = new culture.Calendar(culture.TemporalType.instant, calendarData);
		var instant = calendar.get(2457948.5);
		expect(instant.year).toEqual(2017);
		expect(instant.yearMonth).toEqual(6);
        expect(instant.monthDay).toEqual(13);
    });

	it("load culture", function () {
		var provider = new n.DirctoryCultureDataProvider(data.dataDirectory);
		var cultureData = provider.getCultureSync("en-US");
		expect(cultureData.id).toEqual("en-US");

		var c = new culture.Culture(cultureData);
		var instant = c.parseInstant("2017-07-14 13:14:15");
		expect(instant.year).toEqual(2017);
		expect(instant.yearMonth).toEqual(6);
        expect(instant.monthDay).toEqual(13);
    });
});
