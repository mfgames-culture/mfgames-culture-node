const expect = require("expect");
import * as path from "path";
import * as n from "../index";
import * as culture from "mfgames-culture";
import * as data from "mfgames-culture-data";

describe(path.basename(__filename), function() {
    it("load calendar", function (done) {
		var provider = new n.DirctoryCultureDataProvider(data.dataDirectory);

		provider
			.getCalendarPromise("gregorian")
			.then(calendarData => {
				expect(calendarData.id).toEqual("gregorian");

				var calendar = new culture.Calendar(culture.TemporalType.instant, calendarData);
				var instant = calendar.get(2457948.5);
				expect(instant.year).toEqual(2017);
				expect(instant.yearMonth).toEqual(6);
		        expect(instant.monthDay).toEqual(13);

				done();
			});
    });

	it("load culture", function (done) {
		var provider = new n.DirctoryCultureDataProvider(data.dataDirectory);

		provider
			.getCulturePromise("en-US")
			.then(cultureData => {
				expect(cultureData.id).toEqual("en-US");

				var c = new culture.Culture(cultureData);
				var instant = c.parseInstant("2017-07-14 13:14:15");
				expect(instant.year).toEqual(2017);
				expect(instant.yearMonth).toEqual(6);
		        expect(instant.monthDay).toEqual(13);

				done();
			});
    });
});
