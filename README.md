# MfGames Culture for Node

Provides additional Node features and functionality for [MfGames Culture](https://mfgames.com/mfgames-culture/).

## Installation

Download node at [nodejs.org](http://nodejs.org) and install it, if you haven't already.

```sh
npm install mfgames-culture-node --save
```

## Tests

```sh
npm test
```

## License

MIT

## Examples

Below is an example of using the library to load from a directory. This assumes that every identifier is saved as a JSON file in the top-level of the directory (e.g., `gregorian.js` and `en-US.json`).

```javascript
import * as culture from "mfgames-culture";
import * as cultureNode from "mfgames-culture-node";
import * as data from "mfgames-culture-data";

// Create the provider.
var provider = new cultureNode.DirctoryCultureDataProvider(data.dataDirectory);

// Load a calendar while blocking.
var calendarData = provider.getCalendarSync("gregorian");
var calendar = new culture.Calendar(culture.TemporalType.instant, calendarData);
var instant = calendar.get(2457948.5);

// Load a calendar through a promise.
provider
	.getCalendarPromise("gregorian")
	.then(calendarData => {
		var calendar = new culture.Calendar(culture.TemporalType.instant, calendarData);
		var instant = calendar.get(2457948.5);
	});

// Loading a culture while blocking.
var cultureData = provider.getCultureSync("en-US");
var culture = new culture.Culture(cultureData);
var instant = culture.parseInstant("2017-07-14 13:14:15");

// Loading cultures via promises.
provider
	.getCulturePromise("en-US")
	.then(cultureData => {
		var culture = new culture.Culture(cultureData);
		var instant = culture.parseInstant("2017-07-14 13:14:15");
	});
```
